import pytest
from src.utils.major_time_delta import MajorTimeDelta
from datetime import datetime
from dateutil.relativedelta import relativedelta


def test_years_step():
    first_timepoint = datetime(2019, 7, 14, 12, 30)
    second_timepoint = datetime(2020, 10, 19, 12, 30)
    delta = MajorTimeDelta(second_timepoint, first_timepoint)
    assert delta
    assert delta.get_step() == relativedelta(years=1)


def test_years_print():
    first_timepoint = datetime(2019, 7, 14, 12, 30)
    second_timepoint = datetime(2021, 7, 17, 12, 30)
    delta = MajorTimeDelta(second_timepoint, first_timepoint)
    assert delta

    notification = delta.format_notification("рак свистнет")
    assert  notification == "Через 2 год(а) рак свистнет."


def test_months_step():
    first_timepoint = datetime(2019, 7, 14, 12, 30)
    second_timepoint = datetime(2019, 10, 19, 12, 30)
    delta = MajorTimeDelta(second_timepoint, first_timepoint)
    assert delta
    assert delta.get_step() == relativedelta(months=1)


def test_months_print():
    first_timepoint = datetime(2019, 7, 14, 12, 30)
    second_timepoint = datetime(2019, 10, 14, 12, 30)
    delta = MajorTimeDelta(second_timepoint, first_timepoint)
    assert delta

    notification = delta.format_notification("рак свистнет")
    assert  notification == "Через 3 месяц(ев) рак свистнет."


def test_days_step():
    first_timepoint = datetime(2019, 7, 14, 12, 30)
    second_timepoint = datetime(2019, 7, 17, 11, 30)
    delta = MajorTimeDelta(second_timepoint, first_timepoint)
    assert delta
    assert delta.get_step() == relativedelta(days=1)


def test_days_print():
    first_timepoint = datetime(2019, 7, 14, 10, 30)
    second_timepoint = datetime(2019, 7, 16, 11, 30)
    delta = MajorTimeDelta(second_timepoint, first_timepoint)
    assert delta
    notification = delta.format_notification("рак свистнет")
    assert  notification == "Через 2 суток рак свистнет."


def test_hours_step():
    first_timepoint = datetime(2019, 7, 14, 11, 30)
    second_timepoint = datetime(2019, 7, 14, 12, 30)
    delta = MajorTimeDelta(second_timepoint, first_timepoint)
    assert not delta


def test_hours_print():
    first_timepoint = datetime(2019, 7, 14, 11, 30)
    second_timepoint = datetime(2019, 7, 14, 12, 30)
    delta = MajorTimeDelta(second_timepoint, first_timepoint)
    assert not delta
    notification = delta.format_notification("рак свистнет")
    assert  notification == "Совсем скоро рак свистнет."

def test_negative():
    first_timepoint = datetime(2019, 7, 14, 10, 30)
    second_timepoint = datetime(2019, 7, 14, 11, 30)
    delta = MajorTimeDelta(second_timepoint, first_timepoint)
    assert not delta
    notification = delta.format_notification("рак свистнет")
    assert  notification == "Совсем скоро рак свистнет."
