# TELEGRAM TIMER

Бот для telegram, служащий таймером для напоминания о каких-либо
приближении какого-то события.
## Содержание

1. [Dependencies](#Dependencies)
2. [Настройка](#Setup)
3. [Запуск](#Start)

## <a name="Dependencies"></a>Зависимости
Приложения предоставляет docker контейнеры для запуска. Однако
для отладочного запуска необходимы:

* RabbitMQ
* Postgres или MySQL

Для развертывания в production конфигурации дополнительно необходимо:

* Web сервер с uwsgi

## <a name="Setup"></a>Настройка

Для конфигурации в докер контейнерах необходимо наличие файла
_/home/telegram_timer/data/config.json_
со следующим содержимым:

    {
        "bot_token": "SOME_BOT_TOKEN",
        "broker": "amqp://telegram_timer:test@rabbitmq/telegram_timer",
        "webhook_host": "example.org",
        "database_connection": "postgresql://telegram_timer:test@postgres/telegram_timer",
        "input_timezone": "Europe/Minsk"
    }

В поле **bot_token** необходимо задать токен бота telegram.  
Поле **broker** необходимо сконфигурировать в соответсвии с параметрами
RABBITMQ в настройках окружения.
Правила кофигурации строки брокера смотрите в
[документации Celery](https://docs.celeryproject.org/en/latest/userguide/configuration.html#conf-broker-settings).  
**database_connection** необходимо сконфигурировать в соответсвии с параметрами POSTGRESS.
Правила кофигурации строки подключения к БД смотрите в
[документации sqlalchemy](https://docs.sqlalchemy.org/en/13/core/engines.html#sqlalchemy.create_engine).  
**host** в случаи запуска для отладки можно оставить пустым.

## <a name="Start"></a>Запуск

### Production

Для запуска в production конфигурации необходимо запустить контейнеры
**worker** и **server**. Команды запусков контейнеров не
 автоматизированны для локальной настройки с примерно следующий конфигурацией:

    telegram_timer_worker:
        build:
            context: .
            dockerfile: images/worker/Dockerfile
        depends_on:
            - rabbitmq
        volumes:
            - ./data:/home/telegram_timer/data:ro
        command: celery -A src.worker worker

    telegram_timer_server:
        build:
            context: .
            dockerfile: images/server/Dockerfile
        depends_on:
            - rabbitmq
        volumes:
            - ./data:/home/telegram_timer/data:ro
        command: uwsgi --socket 0.0.0.0:5000 --protocol=uwsgi -w src.server.wsgi:flask_app --master --uid telegram_timer

Параметры запуска celery для контейнера worker смотрите в
[документации Celery](https://docs.celeryproject.org/en/latest/userguide/workers.html#starting-the-worker).  
Параметры uwsgi смотрите в
[документации](https://uwsgi-docs.readthedocs.io/en/latest/).  
Для работы необходимо наличие RabbitMQ. А также вебсервер,
который будет посылать данные через uwsgi.

### Отладка

Для запуска в отладочном режиме вместо образа server можно
использовать образ polling. Для этого образа команды запуска уже
настроенны.
