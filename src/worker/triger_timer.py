"""Contains triger_timer method."""

from datetime import datetime

from ..entities import Timer
from ..utils import MajorTimeDelta

from .cache import g_cache


def triger_timer(task, timer_id):
    """Notify users about events."""

    bot = g_cache.get_bot()
    with g_cache.open_database_session() as session:
        timer = session.query(Timer).filter_by(id=timer_id).first()
        if timer is None:
            return

        now = datetime.utcnow()
        data = timer.timer_data

        finished = now >= data.time_point
        if finished:
            bot.send_message(timer.chat_id, data.final_msg)
            session.delete(timer)
            return

        delta = MajorTimeDelta(data.time_point, now)
        msg = delta.format_notification(data.notify_msg)
        bot.send_message(timer.chat_id, msg)

        if not delta:
            next_triger_time = data.time_point
        else:
            next_triger_time = now + delta.get_step()

        result = task.apply_async(
            args=(timer_id,),
            kwargs=None,
            eta=next_triger_time
        )
        timer.notify_task_id = result.id
