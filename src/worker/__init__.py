"""Contains main bot celery task."""

from celery import Celery

from .. import config

celery = Celery('tasks', broker=config.BROKER)  # pylint: disable=invalid-name


@celery.task(ignore_result=True)
def process_new_updates(json_string):
    """Main message process function."""
    from .cache import g_cache # pylint: disable=import-outside-toplevel,cyclic-import
    import telebot  # pylint: disable=import-outside-toplevel,cyclic-import
    update = telebot.types.Update.de_json(json_string)
    g_cache.get_bot().process_new_updates([update])


@celery.task(ignore_result=True)
def notify(timer_id):
    """Notify users about events."""
    from .triger_timer import triger_timer  # pylint: disable=import-outside-toplevel,cyclic-import
    triger_timer(notify, timer_id)
