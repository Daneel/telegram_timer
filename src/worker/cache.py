"""Contains bot cache class."""

from contextlib import contextmanager

import telebot
from sqlalchemy.orm import sessionmaker

from .. import config
from ..entities import create_engine
from ..message_handler.command_setuper import CommandSetuper


class Cache():
    """Cache bot instance."""

    __slots__ = ("__bot", "__database_engine", "__session_class")

    def __init__(self):
        self.__bot = None
        self.__database_engine = None
        self.__session_class = None

    def get_bot(self):
        """Return cached telegram bot instance."""
        if self.__bot is None:
            self.__bot = telebot.TeleBot(config.BOT_TOKEN, threaded=False)
            self.__database_engine = create_engine()
            self.__session_class = sessionmaker(bind=self.__database_engine)
            CommandSetuper(self.__bot, self.__session_class).setup()
        return self.__bot

    @contextmanager
    def open_database_session(self):
        """"Return context manager for database session."""
        session = self.__session_class()
        yield session
        session.commit()

g_cache = Cache()  # pylint: disable=invalid-name
