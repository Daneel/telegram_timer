"""Contains CreationState entity"""


from sqlalchemy import Column, Integer, ForeignKey, BigInteger
from sqlalchemy.orm import relationship

from .creation_step import CreationStep
from .base_entity import BaseEntity


class CreationState(BaseEntity):
    """Represent data when timer creation is in process."""

    __tablename__ = 'creation_states'

    id = Column(Integer, primary_key=True)
    chat_id = Column(BigInteger, nullable=False)
    user_id = Column(Integer, nullable=False)
    step = Column(
        Integer,
        nullable=False,
        default=CreationStep.TIME_POINT_INPUT
    )

    timer_data_id = Column(
        Integer,
        ForeignKey('timer_datas.id'),
        nullable=False,
    )
    timer_data = relationship("TimerData", foreign_keys=[timer_data_id])

    def __init__(self, chat_id, user_id, timer_data):
        super().__init__()
        self.chat_id = chat_id
        self.user_id = user_id
        self.timer_data = timer_data

    def __repr__(self):
        fmt = "<CreationState(chat_id='{}', user_id='{}', step={}, data={})>"
        return fmt.format(
            self.chat_id,
            self.user_id,
            self.step,
            self.timer_data,
        )
