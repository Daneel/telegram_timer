"""Cointains ChatTimerId entity."""

from sqlalchemy import Column, Integer, BigInteger

from .base_entity import BaseEntity


class ChatTimerId(BaseEntity):
    """Uses to generate unique in chat id."""

    __tablename__ = 'chat_timer_ids'

    id = Column(Integer, primary_key=True)
    chat_id = Column(BigInteger, nullable=False)
    next_id = Column(Integer, nullable=False)

    def __init__(self, chat_id):
        super().__init__()
        self.chat_id = chat_id
        self.next_id = 1

    def __repr__(self):
        fmt = "<ChatTimerId(id='{}', chat_id='{}', next_id={})>"
        return fmt.format(
            self.id,
            self.chat_id,
            self.next_id,
        )
