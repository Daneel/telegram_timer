"""Contains Timer entity."""

from sqlalchemy import Column, Integer, String, ForeignKey, BigInteger
from sqlalchemy.orm import relationship

from .base_entity import BaseEntity


class Timer(BaseEntity):
    """Represent active timer."""

    __tablename__ = 'timers'

    id = Column(Integer, primary_key=True)
    chat_timer_id = Column(Integer, nullable=False)
    chat_id = Column(BigInteger, nullable=False)
    notify_task_id = Column(String, nullable=True)

    timer_data_id = Column(
        Integer,
        ForeignKey('timer_datas.id'),
        nullable=False,
    )
    timer_data = relationship(
        "TimerData",
        foreign_keys=[timer_data_id],
        cascade="all,delete",
    )

    def __init__(self, chat_timer_id, chat_id, timer_data):
        super().__init__()
        self.chat_timer_id = chat_timer_id
        self.chat_id = chat_id
        self.timer_data = timer_data

    def __repr__(self):
        fmt = "<Timer(id='{}'', chat_id='{}'', chat_timer_id='{}')>"
        return fmt.format(
            self.id,
            self.chat_id,
            self.chat_timer_id,
        )
