"""Contains database entities."""


from time import sleep

from sqlalchemy import create_engine as sqlalchemy_create_engine
from sqlalchemy.ext.declarative import declarative_base

from ..config import DATASBASE_CONNECTION

from .base_entity import BaseEntity
from .chat_timer_id import ChatTimerId
from .creation_state import CreationState
from .creation_step import CreationStep
from .timer_data import TimerData
from .timer import Timer


def create_engine():
    """Connect to database. Returns database engine."""

    connected = False
    while not connected:
        try:
            engine = sqlalchemy_create_engine(DATASBASE_CONNECTION, echo=False)
            BaseEntity.metadata.create_all(engine)
            connected = True
        except Exception:  # pylint: disable=broad-except
            print('ERROR: failed to configure database. Retry after 3 sec.')
            sleep(3.0)

    return engine
