"""Initialize BaseEntity by declarative_base instance."""

from sqlalchemy.ext.declarative import declarative_base


BaseEntity = declarative_base()
