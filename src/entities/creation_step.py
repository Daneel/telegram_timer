"""Contains CreationStep enum."""


class CreationStep():
    """Represent step of user information input."""

    TIME_POINT_INPUT = 1
    NOTIFY_MSG_INPUT = 2
    FINAL_MSG_INPUT = 3
