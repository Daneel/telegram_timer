"""Cointains TimerData entity."""

from sqlalchemy import Column, Integer, String, DateTime

from .base_entity import BaseEntity


class TimerData(BaseEntity):
    """Entity, that stores timer data while create and notify."""

    __tablename__ = 'timer_datas'

    id = Column(Integer, primary_key=True)
    time_point = Column(DateTime, nullable=True)
    notify_msg = Column(String, nullable=True)
    final_msg = Column(String, nullable=True)

    def __repr__(self):
        fmt = "<TimerData(id={}, notify_msg='{}'," \
              " notify_msg='{}', final_msg={})>"
        return fmt.format(
            self.id,
            self.time_point,
            self.notify_msg,
            self.final_msg,
        )
