"""Contains root class of apllication, based on TeleBot.polling method."""


import time

import telebot
from sqlalchemy.orm import sessionmaker

from .. import config
from ..entities import create_engine
from ..message_handler.command_setuper import CommandSetuper


class PollingApp:
    """Root class of apllication, based on TeleBot.polling() method."""
    def __init__(self):
        self.bot = telebot.TeleBot(config.BOT_TOKEN, threaded=True)
        self.database_engine = create_engine()
        self.session_class = sessionmaker(self.database_engine)
        CommandSetuper(self.bot, self.session_class).setup()

    def run(self):
        """Execute main application loop."""
        self.bot.remove_webhook()
        time.sleep(0.1)
        self.bot.polling()
