"""Contains MajorTimeDelta instance."""

from dateutil.relativedelta import relativedelta


class MajorTimeDelta():
    """Helper for calculate major delta between two time points."""

    def __init__(self, finish, start):
        self.delta = self.get_major_time_delta(finish, start)

    def __bool__(self):
        return bool(self.delta)

    @staticmethod
    def get_major_time_delta(finish, start):
        """Return major deltatime two time points."""

        if start >= finish:
            return relativedelta()

        delta = relativedelta(finish, start)
        if delta.years:
            return relativedelta(years=delta.years)
        if delta.months:
            return relativedelta(months=delta.months)
        if delta.days:
            return relativedelta(days=delta.days)

        return relativedelta()

    def get_step(self):
        """Return step of major delta."""

        if self.delta.years:
            return relativedelta(years=1)
        if self.delta.months:
            return relativedelta(months=1)
        if self.delta.days:
            return relativedelta(days=1)

        return relativedelta()

    def format_notification(self, msg):
        """Generate human-readable notification for msg."""

        if self.delta.years:
            return "Через {} год(а) {}.".format(self.delta.years, msg)
        if self.delta.months:
            return "Через {} месяц(ев) {}.".format(self.delta.months, msg)
        if self.delta.days:
            return "Через {} суток {}.".format(self.delta.days, msg)

        return "Совсем скоро {}.".format(msg)
