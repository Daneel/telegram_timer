"""Contains DeleteAllTimer message handler."""

from ...entities import Timer
from ... import worker

from .base_command import BaseCommand


class DeleteAllTimer(BaseCommand):
    """Delete all timers in chat."""

    commands = ['delete_all_timer']

    def process(self, message):
        chat_id = message.chat.id
        with self.open_database_session() as session:
            for timer in session.query(Timer).filter_by(chat_id=chat_id):
                if timer.notify_task_id is not None:
                    task_result = worker.notify.AsyncResult(
                        timer.notify_task_id
                    )
                    task_result.revoke()
                session.delete(timer)
        msg = "All timers successfully deleted"
        self.bot.send_message(message.chat.id, msg)
