"""Contains Help message handler."""


from .base_command import BaseCommand

class Help(BaseCommand):
    """Print help information."""

    commands = ['timer_help', 'timers_help', 'help_timer', 'help_timers']

    def process(self, message):
        msg = 'TelegramTimer bot usages:\n'\
              '/create_timer - create timer\n'\
              '/timer_list - list of active timers ids\n'\
              '/timer_info ID - data of specific timer\n'\
              '/delete_timer ID - delete timer\n'\
              '/delete_all_timer - delete timers in this chat\n'
        self.bot.send_message(message.chat.id, msg)
