"""Contains base command class."""


from contextlib import contextmanager


class BaseCommand:
    """Base command hanlder class."""

    commands = None
    regexp = None
    content_types = ['text']

    def __init__(self, bot, session_class):

        self.bot = bot
        self.session_class = session_class

        self.filters = {
            'commands': self.commands,
            'regexp': self.regexp,
            'func': self.filter,
            'content_types': self.content_types,
        }

    @contextmanager
    def open_database_session(self):
        """Return context manager for database session."""
        session = self.session_class()
        yield session
        session.commit()

    def filter(self, _message):
        """Custom message filter."""
        # pylint: disable=no-self-use
        return True

    def process(self, _message):
        """Process message."""
        raise NotImplementedError
