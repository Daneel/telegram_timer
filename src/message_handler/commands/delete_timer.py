"""Contains CreateTimer message handler."""

import re

from ...entities import Timer
from ... import worker

from .base_command import BaseCommand


class DeleteTimer(BaseCommand):
    """Delete timer."""

    commands = ['delete_timer']

    def process(self, message):
        match_regex = r"\/delete_timer\s+(?P<id>\d+)\s*"
        match = re.match(match_regex, message.text)
        if not match:
            msg = "Invalid parameters. Usages:\n    /delete_timer ID"
            self.bot.send_message(message.chat.id, msg)
            return

        with self.open_database_session() as session:
            chat_timer_id = match["id"]
            chat_id = message.chat.id
            timer = session.query(Timer).\
                filter_by(chat_id=chat_id, chat_timer_id=chat_timer_id).\
                first()

            if not timer:
                msg = "Timer #{} is not found".format(chat_timer_id)
                self.bot.send_message(message.chat.id, msg)
                return

            if timer.notify_task_id is not None:
                task_result = worker.notify.AsyncResult(timer.notify_task_id)
                task_result.revoke()

            session.delete(timer)

        msg = "Timer #{} successfully deleted".format(chat_timer_id)
        self.bot.send_message(message.chat.id, msg)
