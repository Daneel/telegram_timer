"""Contains TimerInfo message handler."""

import re

from ...entities import Timer

from .base_command import BaseCommand


class TimerInfo(BaseCommand):
    """Print info about timer."""

    commands = ['timer_info']

    def process(self, message):
        match_regex = r"\/timer_info\s+(?P<id>\d+)\s*"
        match = re.match(match_regex, message.text)

        if not match:
            msg = "Invalid parameters. Usages:\n    /timer_info ID"
            self.bot.send_message(message.chat.id, msg)
            return

        with self.open_database_session() as session:
            chat_timer_id = match["id"]
            chat_id = message.chat.id
            timer = session.query(Timer).\
                filter_by(chat_id=chat_id, chat_timer_id=chat_timer_id).\
                first()

            if not timer:
                msg = "Timer #{} is not found".format(chat_timer_id)
                self.bot.send_message(message.chat.id, msg)
                return

            fmt = 'Timer #{} info:\n'\
                  'Time point - {}\n'\
                  'Notify message - {}\n'\
                  'Final message - {}'

            data = timer.timer_data
            msg = fmt.format(
                chat_timer_id,
                data.time_point,
                data.notify_msg,
                data.final_msg,
            )
            self.bot.send_message(message.chat.id, msg)
