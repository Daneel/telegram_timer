"""Contains CreateTimer message handler."""

from .base_command import BaseCommand
from ...entities import CreationState
from ...entities import TimerData


class CreateTimer(BaseCommand):
    """Create new timer."""

    commands = ['create_timer']

    @staticmethod
    def __delete_creation_state(session, chat_id, user_id):
        creation_state = session.\
            query(CreationState).\
            filter_by(chat_id=chat_id, user_id=user_id).\
            first()

        if creation_state:
            session.delete(creation_state.timer_data)
            session.delete(creation_state)

    def process(self, message):
        with self.open_database_session() as session:
            self.__delete_creation_state(
                session,
                message.chat.id,
                message.from_user.id
            )

            timer_data = TimerData()
            session.add(timer_data)

            creation_state = CreationState(
                message.chat.id,
                message.from_user.id,
                timer_data
            )
            session.add(creation_state)

        reply = "Enter time point (/cancel to exit):"
        self.bot.send_message(message.chat.id, reply)
