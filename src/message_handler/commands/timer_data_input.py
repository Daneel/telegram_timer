"""Contains CreateTimer message handler."""

from datetime import datetime

from dateutil.parser import parse as datetime_parser
from dateutil.tz import tzutc
from dateutil.tz import gettz

from .base_command import BaseCommand
from ...entities import CreationState
from ...entities import CreationStep
from ...entities import Timer
from ...entities import ChatTimerId
from ...utils import MajorTimeDelta
from ... import worker
from ... import config


class TimerDataInput(BaseCommand):
    """Input timer data."""

    content_types = ['text']

    @staticmethod
    def __get_creation_state(message, session):
        """Return CreationState for specific user in chat."""
        chat_id = message.chat.id
        user_id = message.from_user.id
        return session.\
            query(CreationState).\
            filter_by(chat_id=chat_id, user_id=user_id).\
            first()

    def filter(self, message):
        with self.open_database_session() as session:
            return self.__get_creation_state(message, session) is not None

    def process(self, message):
        with self.open_database_session() as session:

            if message.text.startswith('/cancel'):
                self.cancel(session, message)
                return

            creation_state = self.__get_creation_state(message, session)

            if creation_state.step == CreationStep.TIME_POINT_INPUT:
                self.input_time_point(message, creation_state)
            elif creation_state.step == CreationStep.NOTIFY_MSG_INPUT:
                self.input_notify_message(message, creation_state)
            elif creation_state.step == CreationStep.FINAL_MSG_INPUT:
                self.input_final_message(message, session, creation_state)
            else:
                assert False, "Invalid creation step"

    def cancel(self, session, message):
        """Cancel timer creation."""

        creation_state = self.__get_creation_state(message, session)
        if creation_state:
            session.delete(creation_state.timer_data)
            session.delete(creation_state)

        reply = "Timer creation is canceled."
        self.bot.send_message(message.chat.id, reply)


    def input_time_point(self, message, creation_state):
        """Process input time point for timer."""
        input_tz = gettz(config.INPUT_TIMEZONE)
        timepoint_default = datetime.now(input_tz)
        try:
            timepoint_local = datetime_parser(
                message.text,
                default=timepoint_default
            )
        except ValueError:
            reply = "Invalid time point! Try again (/cancel to exit):"
            self.bot.send_message(message.chat.id, reply)
            return

        timepoint_utc = timepoint_local.astimezone(tzutc())
        timepoint = timepoint_utc.replace(tzinfo=None)

        reply = "You enter timepoint {}".format(timepoint.isoformat())
        self.bot.send_message(message.chat.id, reply)

        creation_state.timer_data.time_point = timepoint
        creation_state.step = CreationStep.NOTIFY_MSG_INPUT

        time_point = creation_state.timer_data.time_point
        now = datetime.utcnow()
        delta = MajorTimeDelta(time_point, now)
        notification_example = delta.format_notification('..')
        reply = "Continue notify message: {} (/cancel to exit):".format(
            notification_example
        )
        self.bot.send_message(message.chat.id, reply)

    def input_notify_message(self, message, creation_state):
        """Process input notify message."""
        creation_state.timer_data.notify_msg = message.text
        creation_state.step = CreationStep.FINAL_MSG_INPUT

        reply = "Input final message: (/cancel to exit):"
        self.bot.send_message(message.chat.id, reply)

    @staticmethod
    def __fetch_next_chat_timer_id(message, session):
        """Return next timer id for this chat."""
        chat_id = message.chat.id
        timer_id = session.\
                    query(ChatTimerId).\
                    filter_by(chat_id=chat_id).\
                    first()
        if timer_id is None:
            timer_id = ChatTimerId(chat_id)
            session.add(timer_id)

        next_id = timer_id.next_id
        timer_id.next_id += 1
        return next_id

    def input_final_message(self, message, session, creation_state):
        """Process input final message."""

        creation_state.timer_data.final_msg = message.text

        chat_id = message.chat.id
        chat_timer_id = self.__fetch_next_chat_timer_id(message, session)
        timer = Timer(chat_timer_id, chat_id, creation_state.timer_data)

        session.add(timer)
        session.flush()  # to get timer.id

        session.delete(creation_state)

        reply = "Timer #{} was created!".format(timer.chat_timer_id)
        self.bot.send_message(message.chat.id, reply)
        result = worker.notify.apply_async(args=(timer.id,))
        timer.notify_task_id = result.id
