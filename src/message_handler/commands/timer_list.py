"""Contains TimerList message handler."""

from ...entities import Timer

from .base_command import BaseCommand


class TimerList(BaseCommand):
    """Print list of timer in this chat."""

    commands = ['timer_list']

    def process(self, message):
        chat_id = message.chat.id

        ids = []
        with self.open_database_session() as session:
            for timer in session.query(Timer).filter_by(chat_id=chat_id):
                ids.append(timer.chat_timer_id)

        msg = "List of active timers in this char:\n{}".format(
            ", ".join(
                ["#{}".format(id) for id in ids]
            )
        )
        self.bot.send_message(message.chat.id, msg)
