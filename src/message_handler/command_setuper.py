"""Contains hanlder setupper class."""

from .base_command_setuper import BaseCommandSetuper

from .commands import TimerDataInput
from .commands import CreateTimer
from .commands import DeleteTimer
from .commands import TimerList
from .commands import TimerInfo
from .commands import DeleteAllTimer
from .commands import Help


class CommandSetuper(BaseCommandSetuper):
    """Aggregate command hanlders for telegram_timer."""
    handlers = [
        TimerDataInput,
        CreateTimer,
        DeleteTimer,
        TimerList,
        TimerInfo,
        DeleteAllTimer,
        Help,
    ]
