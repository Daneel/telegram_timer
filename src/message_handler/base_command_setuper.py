"""Contains base class for command handlers agregation."""


class BaseCommandSetuper:
    """Base class for command handlers agregation."""

    handlers = []

    def __init__(self, bot, session_class):
        self.bot = bot
        self.session_class = session_class

    def __add_hanlder(self, handler_instance):
        @self.bot.message_handler(**handler_instance.filters)
        def process(message):  # pylint: disable=unused-variable
            handler_instance.process(message)

    def setup(self):
        """Add command handlers to bot."""

        for handler_class in self.handlers:
            handler_instance = handler_class(self.bot, self.session_class)
            self.__add_hanlder(handler_instance)
