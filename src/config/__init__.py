"""Contains bot configuration."""

import json

CONFIG_FILE_PATH = "data/config.json"


def __load():
    """Load bots config from CONFIG_FILE_PATH."""
    with open(CONFIG_FILE_PATH) as config_file:
        config = json.load(config_file)
        bot_token = config["bot_token"]
        webhook_host = config["webhook_host"]
        broker = config["broker"]
        database_connection = config["database_connection"]
        input_timezone = config["input_timezone"]
    return bot_token, webhook_host, broker, database_connection, input_timezone


BOT_TOKEN, \
WEBHOOK_HOST, \
BROKER, \
DATASBASE_CONNECTION, \
INPUT_TIMEZONE = __load()
